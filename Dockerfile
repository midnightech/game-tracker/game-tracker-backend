FROM python:3.10-slim
RUN apt-get update -y
RUN apt-get install tk -y
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1
ENV PYTHONPATH=/code
WORKDIR /code/
COPY requirements.txt /code/
RUN pip install --no-cache-dir --upgrade -r requirements.txt
COPY . /code/