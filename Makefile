# COLORS
RED    := $(shell tput -Txterm setaf 1)
GREEN  := $(shell tput -Txterm setaf 2)
YELLOW := $(shell tput -Txterm setaf 3)
WHITE  := $(shell tput -Txterm setaf 7)
RESET  := $(shell tput -Txterm sgr0)
TARGET_MAX_CHAR_NUM=20


include .env


ifeq ($(MAKECMDGOALS), $(filter $(MAKECMDGOALS),start stop restart))
ifndef env
$(error Parâmetro env não definido.)
endif
ifeq ($(filter $(env),dev hom prod),)
$(error Parâmetro env inválido.)
endif
ifeq ($(env),dev)
COMPOSE_FILE_PATH := docker-compose.yml
else ifeq ($(env),hom)
COMPOSE_FILE_PATH := docker-compose.hom.yml
else
COMPOSE_FILE_PATH := docker-compose.prod.yml
endif
endif


START_CMD := docker-compose up -d

## Iniciar os containers
start:
	$(info Make: Iniciando containers de ambiente "$(env)".)
	@$(START_CMD)


exists_containers := $(shell docker ps -qa)
ifneq (,$(subst ",,$(exists_containers)))
ifneq ($(filter $(env),hom prod),)
STOP_CMD := docker-compose stop
else
STOP_CMD := docker-compose down
endif
else
STOP_CMD := echo No containers running
endif

## Parar os containers
stop:
	$(info Make: Parando containers de ambiente "$(env)".)
	@$(STOP_CMD) || true


## Reiniciar os containers
restart:
	$(info Make: Reiniciando containers de ambiente "$(env)".)
	@make -s stop
	@make -s start


ifeq ($(MAKECMDGOALS),create-migration)
ifndef name
CREATE_MIGRATION_CMD := echo '*** ${RED}Parâmetro "name" ${RED}não definido${RESET} ***' && \
echo 'Segue o exemplo:' && \
echo '\t${YELLOW}make create-migration ${GREEN}name=example${RESET}'
else
CREATE_MIGRATION_CMD := docker exec -it game_tracker_backend alembic revision --autogenerate -m ${name} && \
sudo chown -R ${USER}:${USER} game-tracker-migrations
endif
endif

## Criar uma migração
create-migration:
	@$(CREATE_MIGRATION_CMD) || true


## Rodar a migração no banco de dados
update-database:
	@docker exec -it game_tracker_backend alembic upgrade head


exists_images := $(shell docker images game_tracker -q)
ifneq (,$(subst ",,$(exists_images)))
CLEAN_IMAGE_CMD := docker rmi $(exists_images) && echo '${GREEN}Imagens removidas com sucesso${RESET}'
else
CLEAN_IMAGE_CMD := echo '${RED}Sem imagens para remover${RESET}'
endif

## Limpar imagem do backend
clean-image:
	@$(CLEAN_IMAGE_CMD) || true


## Realizar os testes unitários
pytest:
	@docker exec -it game_tracker_backend pytest


ifeq ($(MAKECMDGOALS),log)
ifndef keep
$(error Parâmetro keep não definido.)
endif
ifeq ($(filter $(keep),true false),)
$(error Parâmetro keep inválido.)
endif
ifeq ($(keep),true)
LOG_CMD := docker logs -f game_tracker_backend
else
LOG_CMD := docker logs game_tracker_backend
endif
endif

## Log do container backend
log:
	@$(LOG_CMD)


## Mostrar ajuda
help:
	@echo ''
	@echo 'Uso:'
	@echo '  ${YELLOW}make${RESET} ${GREEN}<opção>${RESET}'
	@echo ''
	@echo 'Opções:'
	@awk '/^[a-zA-Z\-\0-9]+:/ { \
		helpMessage = match(lastLine, /^## (.*)/); \
		if (helpMessage) { \
			helpCommand = substr($$1, 0, index($$1, ":")-1); \
			helpMessage = substr(lastLine, RSTART + 3, RLENGTH); \
			printf "  ${YELLOW}%-$(TARGET_MAX_CHAR_NUM)s${RESET} ${GREEN}%s${RESET}\n", helpCommand, helpMessage; \
		} \
	} \
	{ lastLine = $$0 }' $(MAKEFILE_LIST)
