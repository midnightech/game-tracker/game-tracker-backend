from fastapi import HTTPException
from starlette import status


user_not_exist = HTTPException(
        status_code=status.HTTP_404_NOT_FOUND,
        detail="User not exist",
    )