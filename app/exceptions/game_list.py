from fastapi import HTTPException
from starlette import status


game_in_list = HTTPException(
    status_code=status.HTTP_400_BAD_REQUEST,
    detail="This game is alredy inside of list",
)
