from typing import Optional
from app.routers.authorization import router as authorization_router
from app.routers.igdb import router as igdb_proxy_router
from app.routers.games_lista import router as game_lista_router

from fastapi import FastAPI

app = FastAPI()

app.include_router(authorization_router)
app.include_router(igdb_proxy_router)
app.include_router(game_lista_router,tags=["lista"])


@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.get("/items/{item_id}")
def read_item(item_id: int, q: Optional[str] = None):
    return {"item_id": item_id, "q": q}