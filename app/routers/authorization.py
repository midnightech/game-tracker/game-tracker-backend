
from fastapi import Depends
from app.depedencies.user_depedencies import oauth2_scheme
from fastapi.security import OAuth2PasswordRequestForm
from fastapi import APIRouter
from app.schemas.authorization import delete_token_body, response_refresh, response_sign_in, signup_body
from app.view.authorization import delete_token_view, refresh_token_view, signin_from_access_token_view
from app.view.user import signup_view

router = APIRouter()


@router.post("/signin", response_model=response_sign_in)
async def signin_from_access_token(form_data: OAuth2PasswordRequestForm = Depends()):
    response = await signin_from_access_token_view(form_data.username, form_data.password)
    print("=====Token=====")
    print(response)
    print("==========")
    return response


@router.post("/refresh", response_model=response_refresh)
async def refresh_token(token: str = Depends(oauth2_scheme)):
    return await refresh_token_view(token)


@router.delete("/signout")
async def delete_token(request: delete_token_body):
    return await delete_token_view(request)


@router.post("/signup")
async def signup(request: signup_body):
    return await signup_view(request)
