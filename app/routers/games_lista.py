from fastapi import APIRouter, Depends
from pydantic import BaseModel
from app.depedencies.user_depedencies import get_current_user
from app.models.user import User

from app.schemas.user_list import get_user_list_view_response
from app.view.game_list import add_game_user_list_view, get_user_list_view, remove_game_from_list_view
from typing import List,Optional
router = APIRouter()

class game_request(BaseModel):
    game_id: str
    status: int
    time_to_end: Optional[int]

@router.get("/list/{user_id:int}",response_model=List[get_user_list_view_response])
def get_all_games_lista(user_id):
    return get_user_list_view(user_id)

@router.post("/list",status_code=201)
def add_jogo_lista(game: game_request ,user : User = Depends(get_current_user)):
    return add_game_user_list_view(user.id,game)

@router.put("/list")
def edit_info_game_lista():
    raise "Not Implemented"

@router.delete("/list/{game_list_id:int}")
def delete_jogo_lista(game_list_id,user : User = Depends(get_current_user)):
    return remove_game_from_list_view(game_list_id)
