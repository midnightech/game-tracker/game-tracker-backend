from fastapi import APIRouter, Query
from pydantic import BaseModel
from app.view.igdb import igdb_proxy_view

router = APIRouter()

class requestIgdb(BaseModel):
    query: str 


@router.post("/igdb/{path:path}")
def igdb_proxy(path,request : requestIgdb,request_type : str| None = Query(default='POST')):
    return igdb_proxy_view(request_type,path,request)
    