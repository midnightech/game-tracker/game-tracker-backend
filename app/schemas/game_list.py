from pydantic import BaseModel


class game_request(BaseModel):
    game_id: str
    status: int
    time_to_end: int
