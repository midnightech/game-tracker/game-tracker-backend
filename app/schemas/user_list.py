from psycopg2 import Date
from pydantic import BaseModel
from typing import Optional
from datetime import date

class get_user_list_view_response(BaseModel):
    id: int
    status :  Optional[int]
    game_id : str
    time_to_end : Optional[int]