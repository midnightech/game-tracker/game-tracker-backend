from pydantic import BaseModel, Field


class delete_token_body(BaseModel):
    token: str
    refresh: str


class response_sign_in(BaseModel):
    access_token: str
    refresh_token: str
    token_type: str


class response_refresh(BaseModel):
    access_token: str
    token_type: str


class signup_body(BaseModel):
    email : str = Field(
        min_length=1,
    )
    password : str = Field(
        min_length=6,
    )
    username : str = Field(
        min_length=1,
    )

class signup_response(BaseModel):
    id: int
    email : str
    password : str
    username : str
