from app.schemas.igdb import requestIgdb 

from app.use_case.igdb import igdb_proxy_use_case

def igdb_proxy_view(type : str, path : str, body : requestIgdb ):
    return igdb_proxy_use_case(type,path,body)
