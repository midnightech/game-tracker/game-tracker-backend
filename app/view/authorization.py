from app.schemas.authorization import delete_token_body
from app.use_case.authorization import delete_token_use_case, refresh_token_use_case, signin_from_access_token_use_case
from fastapi.responses import JSONResponse
from starlette import status


async def signin_from_access_token_view(username: str, password: str):
    response = signin_from_access_token_use_case(username, password)
    return {
        "access_token": response[0],
        "refresh_token": response[1],
        "token_type": "bearer"
    }


async def refresh_token_view(token: str):
    response = refresh_token_use_case(token)
    return {
        "access_token": response,
        "token_type": "bearer"
    }


async def delete_token_view(request: delete_token_body):
    response = delete_token_use_case(request)
    return JSONResponse(status_code=status.HTTP_204_NO_CONTENT)
