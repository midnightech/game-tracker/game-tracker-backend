from typing import List
from app.schemas.game_list import game_request
from app.schemas.user_list import get_user_list_view_response
from app.use_case.games_list import add_game_user_list_use_case, get_user_list_use_case, remove_game_from_list_use_case


def get_user_list_view(user_id : int) -> List[get_user_list_view_response]:
    game_list = get_user_list_use_case(user_id)
    return [{
        "id" : game.id,
        "status" : game.status,
        "game_id" : game.game_id,
        "time_to_end" : game.time_to_end
    } for game in game_list]

def add_game_user_list_view(user_id: int, game : game_request):
    new_game = add_game_user_list_use_case(user_id,game)
    return {
        "id" : new_game.id,
        "game_id" : new_game.game_id,
        "status" : new_game.status,
        "time_to_end" : new_game.time_to_end,
    }

def remove_game_from_list_view(game_list_id : int):
    return remove_game_from_list_use_case(game_list_id)