
from fastapi.responses import JSONResponse
from starlette import status
from app.schemas.authorization import signup_body
from app.use_case.user import signup_use_case


async def signup_view(user_body : signup_body):
    response = signup_use_case(user_body)
    return JSONResponse(status_code=status.HTTP_201_CREATED, content={
        "id": response.id,
        "username": response.username,
        "email": response.email,
        "activated": response.activated
    })
