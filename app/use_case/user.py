from app.repository.user import create_user, get_user_with_id
from app.schemas.authorization import signup_body
import bcrypt

def signup_use_case(user_body: signup_body):
    user_body.password = bcrypt.hashpw(user_body.password.encode('utf-8'), bcrypt.gensalt()).decode('utf-8')
    user = create_user(user_body)
    return user

def verify_user_exist(user_id):
    response = get_user_with_id(user_id)
    return response != None