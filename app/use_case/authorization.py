

from datetime import datetime, timedelta
from fastapi import HTTPException
from app.depedencies.secrets import ALGORITHM, SECRET_KEY
from app.models.token_black_list import Token_Black_List
from app.models.user import User
from app.repository.token import add_token_to_black_list, get_token_in_black_list
from app.repository.user import get_user_with_username
from jose import JWTError, jwt
from starlette import status
from app.schemas.authorization import delete_token_body
import bcrypt

ACCESS_TOKEN_EXPIRE_MINUTES = 30
# openssl rand -hex 32


def authenticate_user(username : str, password : str):
    user = get_user_with_username(username)
    if not user:
        return False
    if not bcrypt.checkpw(password.encode("utf-8"), user.password.encode("utf-8")):
        return False
    return user


def token_in_black_list(token: str):
    response = get_token_in_black_list(token)
    return not (response == None)


def generate_auth_token(username: str):
    access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    return create_access_token(data={"sub": username, "type": "auth"}, expires_delta=access_token_expires)


def generate_refresh_token(username: str):
    return create_access_token(data={"type": "refresh", "sub": username}, expires_delta=timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES * 2))


def generate_auth_refresh_token(username: str):
    access_token = generate_auth_token(username)
    refresh_token = generate_refresh_token(username)
    return (access_token, refresh_token)


def create_access_token(data: dict, expires_delta: timedelta | None = None):
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=15)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt


def signin_from_access_token_use_case(username: str, password: str):
    user = authenticate_user(username,password)
    print(user)
    if user != False:
        return generate_auth_refresh_token(user.username)

    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"}
        )


def refresh_token_use_case(token: str):
    try:
        if token_in_black_list(token):
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail="Invalid token"
            )
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        if not payload.get("type") == "refresh":
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail="Invalid refresh token",
            )
        user = payload.get("sub")
        return generate_auth_token(user)
    except JWTError:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Invalid refresh token",
        )


def delete_token_use_case(request: delete_token_body):
    add_token_to_black_list([Token_Black_List(
        token=request.token), Token_Black_List(token=request.refresh)])
    return True
