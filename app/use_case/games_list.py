

from app.repository.game_list import add_user_list_repository, get_user_list_repository, get_game_from_user_list_wit_game_id, remove_game_from_list
from app.schemas.game_list import game_request
from app.use_case.user import verify_user_exist
from app.exceptions.user import user_not_exist
from app.exceptions.game_list import game_in_list


def get_user_list_use_case(user_id: int):
    if(verify_user_exist(user_id) == False):
        raise user_not_exist
    return get_user_list_repository(user_id=user_id)


def add_game_user_list_use_case(user_id : int, game: game_request):
    if verify_user_exist(user_id) == False:
        raise user_not_exist
    if get_game_from_user_list_wit_game_id(user_id, game.game_id):
        raise game_in_list

    return add_user_list_repository(user_id, game)

def remove_game_from_list_use_case(game_list_id : int):
    return remove_game_from_list(game_list_id)