from app.repository.idgb_token import get_igdb_token_repository, get_igdb_token_from_igdb_repository, insert_igdb_token, validate_token_repository
from app.schemas.igdb import requestIgdb 
import requests
import os
from dotenv import load_dotenv

load_dotenv()

def get_idgb_token():
    token = get_igdb_token_repository()

    if(token == None or validate_token_repository(token.access_token) == False):
        novo_token = get_igdb_token_from_igdb_repository()
        db_token =insert_igdb_token({
            'access_token' : novo_token["access_token"],
            'expires_in': novo_token['expires_in']
        })
        return db_token
    return token

def igdb_proxy_use_case(type : str, path : str, body : requestIgdb):
    if(type == 'POST'):
        response = requests.post("https://api.igdb.com/v4/" + path,data=body.query,headers={
            "Content-Type" : "text/plain",
            "Client-ID" : os.getenv("IGDB_CLIENT_ID"),
            "Authorization" : "Bearer " + get_idgb_token().access_token
        })
        return response.json()
    raise "Method Invalid"

    

