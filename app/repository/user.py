from fastapi import HTTPException
from starlette import status
from app.models.user import User
from app.schemas.authorization import signup_body
from peewee import IntegrityError


def get_user_with_username(username: str) -> User:
    user = User.get_or_none(User.username == username)
    return user


def get_user_with_id(id: int):
    return User.get_or_none(User.id == id)


def create_user(user_body: signup_body):
    try:
        user = User.create(
            email=user_body.email,
            password=user_body.password,
            username=user_body.username,
            activated=True
        )
        user.save()
        return user
    except (IntegrityError):
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="User alredy has registered"
        )
