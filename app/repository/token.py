from typing import List
from app.models.database import Database
from app.models.token_black_list import Token_Black_List


def get_token_in_black_list(token: str):
    return Token_Black_List.get_or_none(Token_Black_List.token == token)


def add_token_to_black_list(tokens: List[Token_Black_List]):
    Token_Black_List.bulk_create(tokens)
    return True
