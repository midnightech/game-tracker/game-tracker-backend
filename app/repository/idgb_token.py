from typing import TypedDict
from app.models.igdb_token import Igdb_Token
import requests
import os
from dotenv import load_dotenv

load_dotenv()


class _addToken(TypedDict):
    access_token: str
    expires_in: int


class _igdb_token(TypedDict):
    access_token: str
    expires_in: str
    token_type: str


def insert_igdb_token(token: _addToken):
    token_db: Igdb_Token | None = Igdb_Token.get_or_none()
    if token_db == None:
        print("### Criando Token Banco")
        new_token = Igdb_Token.create(
            access_token=token["access_token"],
            expires_in=token["expires_in"]
        )
        new_token.save()
        return new_token
    else:
        print("### Atualizando token banco")
        old_token = token_db
        old_token.access_token = token["access_token"]
        old_token.expires_in = token["expires_in"]
        old_token.save()
        return old_token


def get_igdb_token_repository() -> Igdb_Token | None:
    print("### Coletando token Database")
    return Igdb_Token.get_or_none()
    


def validate_token_repository(token: str):
    header = {'Authorization': "OAuth " + token}
    response = requests.get(
        "https://id.twitch.tv/oauth2/validate", headers=header)
    return response.status_code == 200


def get_igdb_token_from_igdb_repository() -> _igdb_token:
    print("### Coletando token twitch")
    query = {
        "grant_type": "client_credentials",
        "client_secret": os.getenv("IGDB_CLIENT_SECRET"),
        "client_id": os.getenv("IGDB_CLIENT_ID")
    }
    response = requests.post(
        "https://id.twitch.tv/oauth2/token", {}, params=query)
    if(response.status_code != 200):
        print(response.json())
        raise "Erro ao coletar token do IGDB"

    response_json = response.json()
    return {
        "access_token": response_json["access_token"],
        "expires_in": response_json["expires_in"],
        "token_type": response_json["token_type"]
    }
