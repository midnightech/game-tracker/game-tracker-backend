from typing import List
from app.models.game import Game
from app.schemas.game_list import game_request


def get_user_list_repository(user_id: int) -> List[Game]:

    db_lista = Game.select().where(Game.user_id == user_id)
    return db_lista if db_lista != None else []


def add_user_list_repository(user_id: int, game: game_request):
    new_game = Game.create(
        game_id=game.game_id,
        status=game.status,
        time_to_end=game.time_to_end,
        user_id=user_id,
    )
    new_game.save()
    return new_game


def get_game_from_user_list_wit_game_id(user_id: int, game_id: str):
    return Game.get_or_none((Game.game_id == game_id) & (Game.user_id == user_id))


def remove_game_from_list(id : int) -> int:
    return Game.delete_by_id(id)