from .database import ModelBase
from peewee import BooleanField,TextField,IntegerField

class Igdb_Token(ModelBase):
    class Meta:
        table_name = "igdb_token"

    id = BooleanField(primary_key=True, default=True)
    access_token = TextField()
    expires_in = IntegerField()