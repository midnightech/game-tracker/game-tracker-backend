from peewee import AutoField,TextField

from .database import ModelBase

class Token_Black_List(ModelBase):
    class Meta:
        table_name = "token_black_list"
    id = AutoField()
    token = TextField()
