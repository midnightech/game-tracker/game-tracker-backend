
from .database import ModelBase
from peewee import IntegerField,TextField,BooleanField,AutoField
class User(ModelBase):
    class Meta:  
        table_name = "user"

    id = AutoField()
    email = TextField(unique=True)
    password = TextField()
    username = TextField(unique=True)
    image_url = TextField(null=True)
    google_id = TextField(null=True)
    activated = BooleanField(default=False)
