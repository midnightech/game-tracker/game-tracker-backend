from playhouse.postgres_ext import PostgresqlExtDatabase
import os
from dotenv import load_dotenv
import logging
from peewee import Model

load_dotenv()

logger = logging.getLogger('peewee')
logger.addHandler(logging.StreamHandler())
logger.setLevel(logging.DEBUG)

SQLALCHEMY_DATABASE_URL = "postgresql://{name}:{password}@{URL}/{db_name}".format(
    name= os.getenv("POSTGRES_USER"),
    password= os.getenv("POSTGRES_PASSWORD"),
    db_name= os.getenv("POSTGRES_NAME"),
    URL= "db:5432"
)

Database = PostgresqlExtDatabase(
    os.getenv("POSTGRES_NAME"),
    user=os.getenv("POSTGRES_USER"),
    password=os.getenv("POSTGRES_PASSWORD"),
    host="db",
    port="5432",
    autorollback=True
)

class ModelBase(Model):
    class Meta:
        database = Database
