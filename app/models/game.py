from peewee import TextField,IntegerField, ForeignKeyField, AutoField
from .database import ModelBase
from.user import User

class Game(ModelBase):
    class Meta:
        table_name = "game"
    id = AutoField()
    status = IntegerField()
    time_to_end = IntegerField(null=True)
    game_id = TextField()
    user_id = ForeignKeyField(User, backref="user") 
