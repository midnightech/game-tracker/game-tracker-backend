from app.models.database import Database
from app.models.game import Game
from app.models.igdb_token import Igdb_Token
from app.models.token_black_list import Token_Black_List
from app.models.user import User
from playhouse.migrate import PostgresqlMigrator

migrator = PostgresqlMigrator(Database)

if __name__ == '__main__':
    Database.create_tables([User,Token_Black_List,Game,Igdb_Token])