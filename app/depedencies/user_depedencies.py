
from fastapi import HTTPException
from fastapi import Depends
from app.depedencies.depedencies import TokenData
from app.depedencies.secrets import ALGORITHM, SECRET_KEY
from app.repository.user import get_user_with_username

from app.use_case.authorization import token_in_black_list
from starlette import status
from jose import jwt, JWTError
from fastapi.security import OAuth2PasswordBearer


oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")


async def get_current_user(token: str = Depends(oauth2_scheme)):
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validade credentials",
        headers={"WWW-Authenticate": "Bearer"}
    )

    try:
        if token_in_black_list(token):
            raise credentials_exception
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        username: str = payload.get("sub")
        if username is None:
            raise credentials_exception
        token_data = TokenData(username=username)
    except JWTError:
        raise credentials_exception

    user = get_user_with_username(token_data.username)
    if user is None:
        raise credentials_exception
    return user
