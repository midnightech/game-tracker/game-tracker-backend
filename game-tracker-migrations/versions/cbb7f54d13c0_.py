"""empty message

Revision ID: cbb7f54d13c0
Revises: 0cf7ff05f529, 0bd7d6e14825
Create Date: 2022-06-09 03:38:42.914732

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'cbb7f54d13c0'
down_revision = ('0cf7ff05f529', '0bd7d6e14825')
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
