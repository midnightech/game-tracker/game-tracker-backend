from fastapi.testclient import TestClient
from jose import jwt
from app.depedencies.secrets import ALGORITHM, SECRET_KEY

from app.main import app

client = TestClient(app)

data = {
    "username" : "adenilson",
    "password" : "123456",
    "email" : "ade@gmail.com"
}

def test_signup():
    response = client.post("/signup",json=data)
    assert response.status_code == 201
    assert response.json() == {
        "id" : 1,
        "username" : "adenilson",
        "email" : "ade@gmail.com",
        "activated" : True
    }

def test_repeated_signup():
    response = client.post("/signup",json=data)
    assert response.status_code == 400


def get_token():
    return client.post("/signin", data={
        "username": "adenilson", "password": "123456"
    },)


def test_get_auth_token():
    response = get_token()
    assert response.status_code == 200
    responseJson = response.json()
    payload = jwt.decode(
        responseJson["access_token"], SECRET_KEY, algorithms=[ALGORITHM])
    assert payload.get("type") == "auth"
    payload_refresh = jwt.decode(
        responseJson["refresh_token"], SECRET_KEY, algorithms=[ALGORITHM])
    assert payload_refresh.get("type") == "refresh"


def test_refresh_token():
    response = get_token()
    response_json = response.json()
    response_refresh = client.post("/refresh", headers={
        "Authorization": "Bearer " + response_json["refresh_token"]
    })
    assert response_refresh.status_code == 200
    response_refresh_json = response_refresh.json()
    payload = jwt.decode(
        response_refresh_json["access_token"], SECRET_KEY, algorithms=[ALGORITHM])
    assert payload.get("type") == "auth"


def test_delete_token():
    response = get_token()
    response_json = response.json()
    print(response_json)
    response_delete = client.delete("signout", json={
        "token": response_json["access_token"],
        "refresh": response_json["refresh_token"]
    })

    assert response_delete.status_code == 204


def test_refresh_deleted_token():
    response = get_token()
    response_json = response.json()

    response_delete = client.delete("signout", json={
        "token": response_json["access_token"],
        "refresh": response_json["refresh_token"]
    })

    assert response_delete.status_code == 204

    response_refresh = client.post("/refresh", headers={
        "Authorization": "Bearer " + response_json["refresh_token"]
    })

    assert response_refresh.status_code == 401
