
from fastapi.testclient import TestClient
import time

from app.main import app

client = TestClient(app)


def get_token():
    time.sleep(1)
    return client.post("/signin", data={
        "username": "adenilson", "password": "123456"
    },)


def test_add_game_to_list_no_time():
    user_raw_response = get_token()
    user_response = user_raw_response.json()
    print(user_response)
    response = client.post("/list", json={
        "game_id": "2231",
        "status": 0,
        "time_to_end": None
    }, headers={
        "Authorization": "Bearer " + user_response["access_token"],
        "Content-Type": "application/json"
    })

    assert response.status_code == 201
    response_json = response.json()
    assert response_json["id"] != None


def test_add_game_to_list_with_time():
    user_raw_response = get_token()
    user_response = user_raw_response.json()
    print(user_response)
    response = client.post("/list", json={
        "game_id": "2232",
        "status": 0,
        "time_to_end": 1200
    }, headers={
        "Authorization": "Bearer " + user_response["access_token"],
        "Content-Type": "application/json"
    })

    assert response.status_code == 201
    response_json = response.json()
    assert response_json["id"] != None


def test_add_game_to_list_no_auth():
    response = client.post("/list", json={
        "game_id": "2233",
        "status": 0,
        "time_to_end": None
    }, headers={
        "Content-Type": "application/json"
    })

    assert response.status_code == 401


def test_add_repeated_game_to_list():
    user_raw_response = get_token()
    user_response = user_raw_response.json()
    print(user_response)
    response = client.post("/list", json={
        "game_id": "2231",
        "status": 0,
        "time_to_end": None
    }, headers={
        "Authorization": "Bearer " + user_response["access_token"],
        "Content-Type": "application/json"
    })

    assert response.status_code == 400


def test_add_game_to_list_no_game_id():
    user_raw_response = get_token()
    user_response = user_raw_response.json()
    print(user_response)
    response = client.post("/list", json={
        "status": 0,
        "time_to_end": None
    }, headers={
        "Authorization": "Bearer " + user_response["access_token"],
        "Content-Type": "application/json"
    })

    assert response.status_code == 422


def test_get_user_list():
    response_raw = client.get("/list/1")
    response_json = response_raw.json()
    assert response_raw.status_code == 200
    assert len(response_json) > 0


def test_get_user_list_user_not_exist():
    response_raw = client.get("/list/2")
    assert response_raw.status_code == 404


def test_get_user_list_empty_list():
    client.post("/signup", json={
        "username": "adenilson2",
        "password": "123456",
        "email": "ade2@gmail.com"
    })

    response_raw = client.get("/list/3")
    response_json = response_raw.json()
    assert response_raw.status_code == 200
    assert len(response_json) == 0


def test_delete_game_from_list():
    user_raw_response = get_token()
    user_response = user_raw_response.json()
    response = client.delete("/list/1", headers={
        "Authorization": "Bearer " + user_response["access_token"],
    })
    response.status_code == 200


def test_delete_game_from_list_no_auth():
    response = client.delete("/list/1", headers={})
    response.status_code == 401
