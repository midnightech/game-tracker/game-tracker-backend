# from fastapi.testclient import TestClient

from app.models import Database


stmt = Database.execute_sql(
    """SELECT table_name
    FROM information_schema.tables
    WHERE table_schema = 'public' 
    ORDER BY table_name;
    """
)
print(stmt)
results = [x[0] for x in stmt]

def test_database_has_table_user():
    print(results)
    assert results.__contains__('user')


def test_database_has_table_game():
    print(results)
    assert results.__contains__('game')


def test_database_has_table_token_black_list():
    print (results)
    assert results.__contains__('token_black_list')