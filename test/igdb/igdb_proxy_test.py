from fastapi.testclient import TestClient

from app.main import app

client = TestClient(app)

def test_proxy_igdb():
    response = client.post("/igdb/games",json={
        "query" : "fields id; limit 1;"
    })
    assert response.status_code == 200